from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils.text import slugify

from boards.models import Board, Thread, Post


@receiver(pre_save, sender=Board)
def add_slug_to_board(sender, instance, *args, **kwargs):
    if instance and not instance.slug:
        slug = slugify(instance.short_name)
        instance.slug = slug


@receiver(pre_save, sender=Thread)
def add_slug_to_thread(sender, instance, *args, **kwargs):
    if instance and not instance.slug:
        if not instance.title:
            instance.slug = slugify(instance.content)[0:20]
        else:
            instance.slug = slugify(instance.title)


@receiver(post_save, sender=Thread)
def create_thumbnail_for_thread(sender, instance, *args, **kwargs):
    if instance:
        instance.thumbnail.url


@receiver(post_save, sender=Post)
def create_thumbnail_for_post(sender, instance, *args, **kwargs):
    if instance and instance.image:
        instance.thumbnail.url


@receiver(post_save, sender=Post)
def update_thread_date_by_post(sender, instance, *args, **kwargs):
    if instance and instance.created_at:
        thread = Thread.objects.get(posts__id=instance.id)
        thread.updated_by_post = instance.created_at
        thread.save()