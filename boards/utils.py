import os
from django.utils.text import slugify
from PIL import Image

def get_thread_image_path(instance, filename):
    if not instance.title:
        return os.path.join(str(instance.board.slug), slugify(instance.content)[0:20], filename)
    return os.path.join(str(instance.board.slug), slugify(instance.title), filename)


def get_post_image_path(instance, filename):
    return os.path.join(
        str(instance.thread.board.slug),
        slugify(instance.thread.content),
        "posts",
        filename,
    )
