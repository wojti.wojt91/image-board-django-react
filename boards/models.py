from django.db import models
from django.utils import timezone
from .utils import get_thread_image_path, get_post_image_path
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit, Transpose


class Board(models.Model):
    name = models.CharField(max_length=240)
    short_name = models.CharField(max_length=240)
    description = models.TextField()
    slug = models.SlugField(max_length=255, unique=True, blank=True, null=True)

    def __str__(self):
        return f"{self.short_name} - {self.name}"


class Thread(models.Model):
    title = models.CharField(max_length=240, blank=True)
    content = models.TextField()
    image = models.ImageField(upload_to=get_thread_image_path, blank=False)
    thumbnail = ImageSpecField(source='image',
                                      processors=[Transpose(), ResizeToFit(256, 256)],
                                      format='JPEG',
                                      options={'quality': 90})
    board = models.ForeignKey(Board, on_delete=models.CASCADE, related_name="threads")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_by_post = models.DateTimeField(editable=True, blank=True, null=True)
    slug = models.SlugField(max_length=255, unique=True, blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.updated_by_post:
            self.updated_by_post = timezone.now()
        return super(Thread, self).save(*args, **kwargs)

    def __str__(self):
        return f"{self.content} - {self.board.short_name}"



class Post(models.Model):
    content = models.TextField()
    image = models.ImageField(upload_to=get_post_image_path, blank=True)
    thumbnail = ImageSpecField(source='image',
                                      processors=[ResizeToFit(256, 256)],
                                      format='JPEG',
                                      options={'quality': 90})
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE, related_name="posts")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return (
            f"{self.content} - {self.thread.content} - {self.thread.board.short_name}"
        )
