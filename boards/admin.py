from django.contrib import admin

from boards.models import Board, Thread, Post


admin.site.register([Board, Thread, Post])
