from rest_framework import serializers

from boards.models import Thread, Post, Board


class BoardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Board
        fields = "__all__"


class ThreadSerializer(serializers.ModelSerializer):
    replies_count = serializers.SerializerMethodField()
    slug = serializers.SlugField(read_only=True)
    board_slug = serializers.SerializerMethodField()
    created_at = serializers.SerializerMethodField()
    thumbnail = serializers.SerializerMethodField()
    image_name = serializers.SerializerMethodField()

    class Meta:
        model = Thread
        fields = "__all__"

    def get_replies_count(self, instance):
        return instance.posts.count()

    def get_board_slug(self, instance):
        return instance.board.slug

    def get_created_at(self, instance):
        return instance.created_at.strftime("%b. %d, %Y %H:%M:%S")

    def get_thumbnail(self, instance):
        request = self.context.get("request")
        http_or_https = request.is_secure() and "https://" or "http://"
        return http_or_https + request.META["HTTP_HOST"] + instance.thumbnail.url

    def get_image_name(self, instance):
        return str(instance.image.name)


class PostSerializer(serializers.ModelSerializer):
    thread_slug = serializers.SerializerMethodField()
    slug = serializers.SlugField(read_only=True)
    created_at = serializers.SerializerMethodField()
    thumbnail = serializers.SerializerMethodField()
    image_name = serializers.SerializerMethodField()

    class Meta:
        model = Post
        exclude = [
            "thread",
        ]

    def get_thread_slug(self, instance):
        return instance.thread.slug

    def get_created_at(self, instance):
        return instance.created_at.strftime("%b. %d, %Y %H:%M:%S")

    def get_thumbnail(self, instance):
        if instance.image:
            request = self.context.get("request")
            http_or_https = request.is_secure() and "https://" or "http://"
            return http_or_https + request.META["HTTP_HOST"] + instance.thumbnail.url
        return None

    def get_image_name(self, instance):
        return str(instance.image.name)
