from django.urls import include, path
from rest_framework.routers import DefaultRouter

from boards.api import views


router = DefaultRouter()
router.register(r"boards", views.BoardListAPIView)

urlpatterns = [
    path("", include(router.urls)),
    path(
        "boards/<slug:slug>/threads/",
        views.ThreadListAPIView.as_view(),
        name="thread-list",
    ),
    path(
        "boards/<slug:slug>/threads/create/",
        views.ThreadCreateAPIView.as_view(),
        name="thread-create",
    ),
    path(
        "thread/<slug:slug>/", views.SingleThreadAPIView.as_view(), name="single-thread"
    ),
    path(
        "thread/<slug:slug>/posts/", views.PostListAPIView.as_view(), name="post-list"
    ),
    path(
        "thread/<slug:slug>/posts/create/",
        views.PostCreateAPIView.as_view(),
        name="post-create",
    ),
]
