from rest_framework import generics, viewsets
from rest_framework.generics import get_object_or_404

from .permissions import IsAdminUserOrReadOnly
from .serializers import ThreadSerializer, PostSerializer, BoardSerializer
from .pagination import ThreadPagination
from boards.models import Thread, Post, Board


class BoardListAPIView(viewsets.ModelViewSet):
    serializer_class = BoardSerializer
    queryset = Board.objects.all()
    permission_classes = [IsAdminUserOrReadOnly]


class ThreadListAPIView(generics.ListAPIView):
    serializer_class = ThreadSerializer
    pagination_class = ThreadPagination

    def get_queryset(self):
        kwarg_slug = self.kwargs.get("slug")
        return Thread.objects.filter(board__slug=kwarg_slug).order_by(
            "-updated_by_post"
        )


class SingleThreadAPIView(generics.RetrieveAPIView):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer
    lookup_field = "slug"


class PostListAPIView(generics.ListAPIView):
    serializer_class = PostSerializer

    def get_queryset(self):
        kwarg_slug = self.kwargs.get("slug")
        last_n_posts = self.request.query_params.get("last")
        queryset = Post.objects.filter(thread__slug=kwarg_slug)
        count_posts = queryset.count()
        if last_n_posts is not None and int(last_n_posts) <= count_posts:
            return queryset[(count_posts - int(last_n_posts)) : count_posts]
        return queryset


class ThreadCreateAPIView(generics.CreateAPIView):
    serializer_class = ThreadSerializer
    queryset = Thread.objects.all()

    def perform_create(self, serializer):
        kwarg_slug = self.kwargs.get("slug")
        board = get_object_or_404(Board, slug=kwarg_slug)
        serializer.save(board=board)


class PostCreateAPIView(generics.CreateAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def perform_create(self, serializer):
        kwarg_slug = self.kwargs.get("slug")
        thread = get_object_or_404(Thread, slug=kwarg_slug)
        serializer.save(thread=thread)
