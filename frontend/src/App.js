import { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  useParams,
} from "react-router-dom";
import axios from 'axios';

import NavbarComponent from "./components/navbar/Navbar.component";
import MainPage from "./components/main-page/MainPage.component";
import Board from "./components/board/Board.component";
import SingleThread from "./components/single-thread/SingleThread.component";

import "./App.css";

const App = () => {
  const [boards, setBoards] = useState([]);

  useEffect(() => {
    const fetchBoards = async () => {
      const result = await axios(
        'http://localhost:8000/api/boards/',
      );
 
      setBoards(result.data);
    };
 
    fetchBoards();
  }, []);


  const ShowBoard = () => {
    const { slug } = useParams();
    return <Board boardSlug={slug} key={window.location.pathname} />;
  }

  const ShowThread = () => {
    const { threadSlug } = useParams();
    return <SingleThread threadSlug={threadSlug} key={window.location.pathname} />
  }

  return (
    <div className="background-main">
      <Router>
        <NavbarComponent boards={boards} />
        <Switch>
          <Route exact path="/">
            <MainPage boards={boards} />
          </Route>
          <Route exact path="/:slug/">
            <ShowBoard />
          </Route>
          <Route path="/:slugOfBoard/:threadSlug">
            <ShowThread />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
