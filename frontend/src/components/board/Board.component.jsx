import React from "react";
import { useState, useEffect } from "react";

import { Pagination } from "react-bootstrap";
import axios from "axios";

import Thread from "../thread/Thread.component";

import "./Board.styles.css";

const Board = ({ boardSlug }) => {
  const [threads, setThreads] = useState([]);
  const [totalPages, setTotalPages] = useState(null);
  const [query, setQuery] = useState("");
  const [pageNum, setPageNum] = useState(1);

  useEffect(() => {
    document.title = `/${boardSlug}/ - ImageBoard`
    const fetchThreads = async () => {
      const result = await axios(
        `http://localhost:8000/api/boards/${boardSlug}/threads/${query}`
      );

      setThreads(result.data.results);
      setTotalPages(result.data.total_pages);
    };
    fetchThreads();
  }, [query]);


  const handlePage = (pageNumber) => {
    setQuery(`?page=${pageNumber}`);
    setPageNum(pageNumber);
  };

  const Pages = () => {
    let active = pageNum;
    let items = [];

    for (let number = 1; number <= totalPages; number++) {
      items.push(
        <Pagination.Item
          key={number}
          active={number === active}
          onClick={() => handlePage(number)}
        >
          {number}
        </Pagination.Item>
      );
    }
    return <Pagination size="sm">{items}</Pagination>;
  };

  return (
    <div>
      {threads.map((thread) => (
        <Thread
          key={thread.id}
          threadId={thread.id}
          threadTitle={thread.title}
          threadContent={thread.content}
          threadThumbnail={thread.thumbnail}
          threadImage={thread.image}
          threadRepliesCount={thread.replies_count}
          threadSlug={thread.slug}
          threadBoardSlug={thread.board_slug}
          threadBoard={thread.board}
          threadCreatedAt={thread.created_at}
          threadImageName={thread.image_name}
          maxPosts={"?last=3"}
        />
      ))}
      <div>
        <hr />
        <div className="div-padding"></div>
        <div className="footer-fixed">{totalPages > 1 && <Pages />}</div>
      </div>
    </div>
  );
};

export default Board;
