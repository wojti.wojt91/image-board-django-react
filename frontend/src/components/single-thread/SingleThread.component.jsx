import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import Thread from "../thread/Thread.component";

const SingleThread = ({ threadSlug }) => {
  const [thread, setThread] = useState([]);

  useEffect(() => {
    document.title = `${threadSlug} - ImageBoard`
    const fetchThread = async () => {
      const result = await axios(
        `http://127.0.0.1:8000/api/thread/${threadSlug}/`
      );

      setThread(result.data);
    };
    fetchThread();
  }, []);
  return (
    <div>
      <Thread
        key={thread.id}
        threadId={thread.id}
        threadTitle={thread.title}
        threadContent={thread.content}
        threadThumbnail={thread.thumbnail}
        threadImage={thread.image}
        threadRepliesCount={thread.replies_count}
        threadSlug={thread.slug}
        threadBoardSlug={thread.board_slug}
        threadBoard={thread.board}
        threadCreatedAt={thread.created_at}
        threadImageName={thread.image_name}
        maxPosts={''}
      />
      <hr />
      <div className="div-padding"></div>
    </div>
  );
};

export default SingleThread;
