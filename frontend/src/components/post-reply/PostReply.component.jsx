import React from "react";
import { Media } from "react-bootstrap";
import SimpleReactLightbox from "simple-react-lightbox";
import ImageLightbox from "../image-lightbox/ImageLightbox.component";
import "./PostReply.styles.css";

const PostReply = ({
  postImage,
  postThumbnail,
  postImageName,
  postContent,
}) => {
  return (
    <div className="reply-div">
      <Media>
        <SimpleReactLightbox>
          <ImageLightbox
            image={postImage}
            thumbnail={postThumbnail}
            content={postImageName}
          />
        </SimpleReactLightbox>
        <Media.Body>
          <p>{postContent}</p>
        </Media.Body>
      </Media>
    </div>
  );
};

export default PostReply;
