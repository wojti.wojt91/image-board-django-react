import React from "react";
import { SRLWrapper } from "simple-react-lightbox";
import "./ImageLightbox.styles.css";

const ImageLightbox = ({ image, thumbnail, content }) => {
  const options = {
    thumbnails: {
      showThumbnails: false,
      thumbnailsOpacity: 0.4,
      thumbnailsSize: ["100px", "80px"],
    },
    buttons: {
      backgroundColor: "rgba(30,30,36,0.8)",
      iconColor: "rgba(255, 255, 255, 0.8)",
      iconPadding: "5px",
      showAutoplayButton: false,
      showCloseButton: true,
      showDownloadButton: true,
      showFullscreenButton: true,
      showNextButton: false,
      showPrevButton: false,
      size: "40px",
    },
    caption: {
      showCaption: false,
    }
  };

  return (
    <div className="margin-media">
      <SRLWrapper options={options}>
        <a href={image} data-attribute="SRL">
          <img src={thumbnail} alt={content} className="rounded-img" />
        </a>
      </SRLWrapper>
    </div>
  );
};

export default ImageLightbox;
