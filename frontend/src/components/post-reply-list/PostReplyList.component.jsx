import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import PostReply from "../post-reply/PostReply.component";

const PostReplyList = ({ maxPosts, threadSlug }) => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const fetchPosts = async () => {
      const result = await axios(
        `http://127.0.0.1:8000/api/thread/${threadSlug}/posts/${maxPosts}`
      );

      setPosts(result.data);
    };
    fetchPosts();
  }, []);
  return (
    <div>
      {posts.map((post) => (
        <PostReply
          postImage={post.image}
          postThumbnail={post.thumbnail}
          postImageName={post.image_name}
          postContent={post.content}
          key={post.id}
        />
      ))}
    </div>
  );
};

export default PostReplyList;
