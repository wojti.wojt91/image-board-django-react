import React from 'react'
import { Container } from "react-bootstrap";
import "./MainPage.styles.css";

const MainPage = () => {
    return (
        <Container fluid>
            <h1>Main paige</h1>
        </Container>
    )
}

export default MainPage
