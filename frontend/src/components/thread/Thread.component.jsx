import React from "react";
import { Container, Media } from "react-bootstrap";
import { Link, useRouteMatch } from "react-router-dom";
import SimpleReactLightbox from "simple-react-lightbox";
import ImageLightbox from "../image-lightbox/ImageLightbox.component";
import PostReplyList from "../post-reply-list/PostReplyList.component";
import "./Thread.styles.css";

const Thread = ({
  threadId,
  threadTitle,
  threadContent,
  threadImage,
  threadRepliesCount,
  threadSlug,
  threadBoardSlug,
  threadBoard,
  threadCreatedAt,
  threadThumbnail,
  threadImageName,
  maxPosts,
}) => {
  let { path, url } = useRouteMatch();

  return (
    <Container fluid>
      <hr />
      <Media>
        <SimpleReactLightbox>
          <ImageLightbox
            image={threadImage}
            thumbnail={threadThumbnail}
            content={threadImageName}
          />
        </SimpleReactLightbox>
        <Media.Body>
          <div className="margin-media-body">
            <h5>
              <b>{threadTitle}</b>
            </h5>
            <p>
              Created: {threadCreatedAt}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Replies:{" "}
              {threadRepliesCount}&nbsp;&nbsp;&nbsp;
              {url === "/" + threadBoardSlug && (
                <Link to={`${url}/${threadSlug}`}>View Thread</Link>
              )}
            </p>
            <p>{threadContent}</p>
          </div> 
        </Media.Body>
      </Media>
      <PostReplyList maxPosts={maxPosts} threadSlug={threadSlug} />
    </Container>
  );
};

export default Thread;
