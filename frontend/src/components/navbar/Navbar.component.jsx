import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

import "./Navbar.styles.css";

const NavbarComponent = ({ boards }) => {
  return (
    <Navbar bg="gradient" expand="lg md" sticky="top" variant="dark">
      <LinkContainer to="/" key="home">
        <Navbar.Brand>ImageBoard</Navbar.Brand>
      </LinkContainer>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {boards.map((board) => (
            <LinkContainer to={`/${board.slug}`} key={board.id} id={`/${board.slug}`}>
              <Nav.Link key={`${board.id}child`} eventKey={`${board.id}child`}>{board.short_name}</Nav.Link>
            </LinkContainer>
          ))}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavbarComponent;
